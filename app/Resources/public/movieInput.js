/**
 * Created by monika on 17.4.22.
 */
import React from 'react';
import PropTypes from 'prop-types';

class MovieInput extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div>
                <input type="text" value={this.props.inputValue} onChange={this.props.onValueChange.bind(this)}/>
            </div>
        );
    }
}

MovieInput.propTypes = {
    inputText: PropTypes.string
};

export default MovieInput;

