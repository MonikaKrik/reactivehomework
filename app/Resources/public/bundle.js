import React from 'react';
import ReactDOM from 'react-dom';
import MovieItem from 'movieItem';
import MovieInput from 'movieInput';

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            movie: {},
            inputValue: ""
        };
        this.onSearch = this.onSearch.bind(this);
    }
    handleErrors(response) {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        return response;
    }
    fetchMovie(movie) {
        fetch(`http://omdbapi.com/?t=${movie}`)
            .then(this.handleErrors)
            .then(response => response.json() )
            .then((json) => this.setState({movie: json}))
            .catch(error => console.log(error));
    }
    componentWillMount(){
        this.fetchMovie('home alone');
    }
    onSearch(e){
        this.setState({inputValue: e.target.value});
        this.fetchMovie(e.target.value);

        // Cleans input after 3 seconds
        setTimeout(function() { this.setState({inputValue: ""}); }.bind(this), 3000);
    }
    render() {
        return (
            <div>
                <MovieItem movie={this.state.movie}/>
                <MovieInput inputValue={this.state.inputValue} onValueChange={this.onSearch}/>
            </div>
        );
    }

}
ReactDOM.render(<App />, document.getElementById('reactApp'), );