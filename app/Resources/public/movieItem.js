/**
 * Created by monika on 17.4.21.
 */
import React from 'react';
import PropTypes from 'prop-types';

const MovieItem = ({movie}) => {
    return (
        <div>
            <img src={movie.Poster} alt="Movie poster"/>
            <h1>{movie.Title}</h1>
            <p>{movie.Genre}</p>
        </div>
    );
};

MovieItem.propTypes = {
    movie: PropTypes.object
};

export default MovieItem;
